#!/bin/bash

# Teste de estresse para verificar o comportamento sob carga pesada

function test_server_stress() {
    ab -n 1000 -c 100 http://localhost:8080/cliente/123 > /dev/null
    assertEquals "Teste de estresse falhou" 0 $?
}

# Executar o teste de estresse
test_server_stress
