#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <microhttpd.h>
#include <pthread.h>
#include <signal.h>
#include <cJSON.h>

#define MAX_CLIENTES 100
#define MAX_TRANSACOES 10

typedef struct {
    char id[20];
    int saldo;
    int status; // 1 se a conexão está em uso, 0 se está livre
    int in_transaction; // 1 se a transação está em andamento, 0 se não está
    cJSON *ultimas_transacoes; // Array JSON das últimas transações do cliente
} cliente;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
cliente clientes[MAX_CLIENTES];

// Funções auxiliares
cliente *get_cliente(const char *id) {
    for (int i = 0; i < MAX_CLIENTES; i++) {
        if (strcmp(clientes[i].id, id) == 0) {
            return &clientes[i];
        }
    }
    return NULL;
}

void init_cliente(cliente *c, const char *id, int saldo) {
    strcpy(c->id, id);
    c->saldo = saldo;
    c->status = 0;
    c->in_transaction = 0;
    c->ultimas_transacoes = cJSON_CreateArray();
}

void free_cliente(cliente *c) {
    cJSON_Delete(c->ultimas_transacoes);
    memset(c, 0, sizeof(cliente));
}

// Implementação de handle_request()
int handle_request(void *cls, struct MHD_Connection *connection, const char *url, const char *method, const char *version,
                   const char *upload_data, size_t *upload_data_size, void **con_cls) {
    if (strcmp(method, "GET") == 0) {
        const char *client_id = strrchr(url, '/') + 1;
        cliente *c = get_cliente(client_id);
        if (c == NULL) {
            return MHD_NO;
        }

        cJSON *response_json = cJSON_CreateObject();
        cJSON_AddItemToObject(response_json, "id", cJSON_CreateString(c->id));
        cJSON_AddItemToObject(response_json, "saldo", cJSON_CreateNumber(c->saldo));
        cJSON_AddItemToObject(response_json, "transacoes", cJSON_Duplicate(c->ultimas_transacoes, 1));

        char *response_data = cJSON_PrintUnformatted(response_json);
        struct MHD_Response *response = MHD_create_response_from_buffer(strlen(response_data), response_data, MHD_RESPMEM_MUST_FREE);
        MHD_add_response_header(response, "Content-Type", "application/json");
        int ret = MHD_queue_response(connection, MHD_HTTP_OK, response);
        MHD_destroy_response(response);
        cJSON_Delete(response_json);

        return ret;
    } else if (strcmp(method, "POST") == 0) {
        const char *client_id = strrchr(url, '/') + 1;
        cliente *c = get_cliente(client_id);
        if (c == NULL) {
            return MHD_NO;
        }

        cJSON *request_json = cJSON_Parse(upload_data);
        if (request_json == NULL) {
            return MHD_NO;
        }

        const cJSON *transacao_json = cJSON_GetObjectItem(request_json, "transacao");
        if (transacao_json == NULL) {
            cJSON_Delete(request_json);
            return MHD_NO;
        }

        cJSON_AddItemToArray(c->ultimas_transacoes, cJSON_Duplicate(transacao_json, 1));

        cJSON_Delete(request_json);

        return MHD_YES;
    }

    return MHD_NO;
}

// Implementação de agora()
void agora(char *dt) {
    time_t rawtime;
    struct tm *info;
    time(&rawtime);
    info = localtime(&rawtime);
    strftime(dt, 80, "%Y-%m-%dT%H:%M:%S", info);
}

// Função de manipulação de sinais
void sig_handler(int signo) {
    if (signo == SIGINT || signo == SIGTERM) {
        printf("\nEncerrando o servidor...\n");
        for (int i = 0; i < MAX_CLIENTES; i++) {
            if (clientes[i].id[0] != '\0') {
                free_cliente(&clientes[i]);
            }
        }
        exit(0);
    }
}

// Função principal
int main() {
    // Inicialização dos clientes
    for (int i = 0; i < MAX_CLIENTES; i++) {
        clientes[i].id[0] = '\0';
    }

    // Inicialização do servidor HTTP
    struct MHD_Daemon *daemon;
    daemon = MHD_start_daemon(MHD_USE_SELECT_INTERNALLY, 8080, NULL, NULL, &handle_request, NULL, MHD_OPTION_END);
    if (daemon == NULL) {
        printf("Erro ao iniciar o servidor.\n");
        return 1;
    }

    // Configuração do tratamento de sinais
    signal(SIGINT, sig_handler);
    signal(SIGTERM, sig_handler);

    printf("Servidor iniciado na porta 8080.\n");

    // Loop principal
    while (1) {
        sleep(1);
    }

    return 0;
}
